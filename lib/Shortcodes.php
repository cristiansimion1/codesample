<?php
namespace CSCodeable\Lib;


class Shortcodes
{
    function __construct()
    {
        add_shortcode( 'table_roles', array( $this, 'roles_list' ) );
    }

    function roles_list( $atts, $content = null )
    {
        global $wp_roles;

        if( !current_user_can( 'list_users' ) )
            return false;

        ob_start();

            require_once CS_CODEABLE_TEMPLATE_DIR . 'table.php';

        return ob_get_clean();
    }
}