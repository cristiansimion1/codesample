<?php
namespace CSCodeable\Lib;

class EnqueueScripts
{
    function __construct()
    {
        add_action('wp_enqueue_scripts', array( $this, 'frontend' ) );
    }

    function frontend()
    {
        wp_register_style ( 'cs-codeable-bootstrap-css', plugins_url( 'css/bootstrap4.min.css', CS_CODEABLE_FILE ) );
        wp_register_style ( 'cs-codeable-css-custom', plugins_url( 'css/custom.css', CS_CODEABLE_FILE ) );
        wp_register_script( 'cs-codeable-bootstrap-js', plugins_url( 'js/bootstrap.bundle.js', CS_CODEABLE_FILE ), array('jquery') );
        wp_register_script( 'cs-codeable-custom', plugins_url( 'js/custom.js', CS_CODEABLE_FILE ), array('jquery') );

        wp_localize_script( 'cs-codeable-custom', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }
}