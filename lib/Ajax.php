<?php
namespace CSCodeable\Lib;


class Ajax
{
    function __construct()
    {
        add_action( 'wp_ajax_cs_user_return_table', array( $this, 'get_users' ) );
        add_action( 'wp_ajax_nopriv_cs_user_return_table', array( $this, 'get_users' ) );
    }

    function get_page()
    {
        check_ajax_referer( '11066e13609c22ee7967561652da1347', 'security' );
    }

    function get_users()
    {
        global $wpdb;

        check_ajax_referer( '11066e13609c22ee7967561652da1347', 'security' );

        if( !is_user_logged_in() )
            wp_send_json( array( 'status' => 'error_permissions' ) );

        if( !current_user_can('list_users') )
            wp_send_json( array( 'status' => 'error_permissions' ) );

        $post      = $_POST;

        $role      = sanitize_text_field( $post["role"] );
        $orderby   = sanitize_text_field( $post["orderby"] );
        $order     = sanitize_text_field( $post["order"] );
        $base      = sanitize_text_field( $post["url_base"] );
        $page      = sanitize_text_field( $post["cs_order_page"] );

        if(!is_numeric($page))
            $page  = 1;

        $args      = array(
            "role"    => $role,
            "orderby" => $orderby,
            "order"   => $order,
            "paged"   => $page
        );

        /** save sorting */
        update_user_meta( get_current_user_id(), 'cs_codeable_settings_saved', json_encode( $args ) );

        $users                  = Users::get_user_by( $args );
        $response["body"]       = Users::display_refresh( $users["results"] );
        $response["pagination"] = Pagination::show_pagination( $users["count"], $page );
        $response["status"]     = "success";

        wp_send_json( $response );
    }
}