<?php
namespace CSCodeable\Lib;


class Users
{
    static function get_roles()
    {
        global $wp_roles;

        return $wp_roles;
    }

    static function get_user_by( $args, $perpage = 10 )
    {
        $defaults          = array(
            'blog_id'      => $GLOBALS['blog_id'],
            'role'         => '',
            'role__in'     => array(),
            'role__not_in' => array(),
            'meta_key'     => '',
            'meta_value'   => '',
            'meta_compare' => '',
            'meta_query'   => array(),
            'date_query'   => array(),
            'include'      => array(),
            'exclude'      => array(),
            'orderby'      => 'login',
            'order'        => 'ASC',
            'offset'       => '',
            'search'       => '',
            'paged'        => '1',
            'number'       => $perpage,
            'count_total'  => true,
            'fields'       => 'all',
            'who'          => '',
        );

        $args              = wp_parse_args( $args, $defaults );
        $users             = new \WP_User_Query ( $args );
        $result["results"] = $users->get_results();

        /** calculate number of pages */
        $total_pages       = ceil( $users->get_total() / $perpage );

        /** return number of pages */
        $result["count"]   = $total_pages;

        return $result;
    }

    static function get_last_query()
    {
        if(!is_user_logged_in())
            return false;
        if(!current_user_can("list_users"))
            return false;

        $paged = get_query_var( 'paged' );
        if(!$paged) $paged = 1;

        $settings = get_user_meta( get_current_user_id(),"cs_codeable_settings_saved", true );

        if(!$settings)
        {
            $settings = array(
                "orderby"  => "ID",
                "order"    => "ASC",
                "paged"    => $paged
            );
        }
        else
        {
            $settings = json_decode( $settings, true );
        }


        return Users::get_user_by( $settings );
    }

    static function display_refresh( $data )
    {
        ob_start();

        if( count( $data ) )
        {
            foreach( $data as $user )
            {
                ?>
                <tr>
                    <td><?php _e( $user->data->ID, 'codeable-test'); ?></td>
                    <td><?php _e( $user->data->user_login, 'codeable-test'); ?></td>
                    <td><?php _e( $user->data->display_name, 'codeable-test'); ?></td>
                    <td><?php _e( rtrim(implode(",", $user->roles),","), 'codeable-test'); ?></td>
                </tr>
                <?php
            }

        }
        else
        {
            echo '<tr><td colspan="4">' . __("No entries found", "codeable-test") . '</td></tr>';
        }

        return ob_get_clean();
    }
}