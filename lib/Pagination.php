<?php
namespace CSCodeable\Lib;


class Pagination
{
    function __construct()
    {
        add_filter( 'cs_custom_pagination', array($this, 'apply_filters'), 10, 2 );
    }

    public function apply_filters( $current_page, $page_total )
    {
        if(!$current_page) $current_page = get_query_var( 'paged' );
        global $wp_query;

        $render = Pagination::show_pagination( $page_total, $current_page );

       return $render;
    }

    static function show_pagination( $pages_total, $current = null )
    {
        if(!$current) $current = 1;
        ob_start();

            global $wp_query;

            if( $pages_total < 2 )
                return ob_get_clean();

            ?>
            <div class="ecbc-bootstrap-wrapper">
                <div class="row justify-content-end">
                    <div class="col-md-12 text-right">
                        <?php if( $current > 1 ): ?>
                            <button class="btn nav_prev_cs btn-outline-secondary" style="min-width: 112px;">Previous</button>
                        <?php endif; ?>
                        <input class="text-center page_number_index" max="<?php echo $pages_total; ?>" style="max-width:80px" type="text" name="page" value="<?php echo $current; ?>" /> of <?php echo $pages_total; ?>
                        <input class="cs_codeable_total_pages" type="hidden" name="cs_codeable_total_pages" value="<?php echo $pages_total; ?>" />
                        <?php if( $current < $pages_total ): ?>
                            <button class="btn nav_next_cs btn-outline-secondary" style="min-width: 112px;">Next</button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php

        return ob_get_clean();
    }
}