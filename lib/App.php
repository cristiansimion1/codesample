<?php
namespace CSCodeable\Lib;

class App
{
    function __construct()
    {
        $ajax               = new Ajax();

        $shortcodes         = new Shortcodes();
        $scripts_and_styles = new EnqueueScripts();

        $pagination         = new Pagination();
    }
}