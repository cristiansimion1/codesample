<?php

wp_enqueue_style  ( 'cs-codeable-bootstrap-css' );
wp_enqueue_style  ( 'cs-codeable-css-custom' );
wp_enqueue_script ( 'cs-codeable-bootstrap-js' );
wp_enqueue_script ( 'cs-codeable-custom' );

$users              = \CSCodeable\Lib\Users::get_last_query();
$page_count         = $users["count"];
$default_sorting    = json_decode( get_user_meta( get_current_user_id(),"cs_codeable_settings_saved", true ), true );
$pages              = ( $default_sorting ) ? $default_sorting["paged"] : null;
?>
<div class="ecbc-bootstrap-wrapper">

    <form class="cs-controller-form" action="" method="post">
        <?php do_action( "cs_before_form" ); ?>
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-secondary cs-reset-form" type="button">Reset</button>
                    </div>
                    <select name="role" class="custom-select cs-role-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                        <option <?php echo ( $default_sorting["role"] ) ? 'selected="selected"' : ""; ?> value=""><?php _e( 'All', 'codeable-test' ); ?></option>
                        <?php foreach( \CSCodeable\Lib\Users::get_roles()->roles as $key => $role ): ?>
                            <option <?php echo ( $default_sorting["role"] == $key ) ? 'selected="selected"' : ""; ?> value="<?php _e( $key , 'codeable-test' ); ?>"><?php _e( $role["name"], 'codeable-test' ); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-md-8 text-right">
                <div class="btn-group">
                    <select name="orderby" class="custom-select cs-orderby-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                        <option  <?php echo ( $default_sorting["orderby"] === "ID" ) ? 'selected="selected"' : ""; ?>  value="ID"><?php _e( 'Order by ID', 'codeable-test' ); ?></option>
                        <option  <?php echo ( $default_sorting["orderby"] === "username" ) ? 'selected="selected"' : ""; ?>  value="username"><?php _e( 'Order by Username', 'codeable-test' ); ?></option>
                        <option  <?php echo ( $default_sorting["orderby"] === "display_name" ) ? 'selected="selected"' : ""; ?>  value="display_name"><?php _e( 'Order by Display name', 'codeable-test' ); ?></option>
                    </select>
                </div>
                <div class="btn-group">
                    <select name="order" class="custom-select cs-order-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                        <option <?php echo ( $default_sorting["order"] === "ASC" ) ? 'selected="selected"' : ""; ?> value="ASC"><?php _e( 'Ascending', 'codeable-test' ); ?></option>
                        <option <?php echo ( $default_sorting["order"] === "DESC" ) ? 'selected="selected"' : ""; ?> value="DESC"><?php _e( 'Descending', 'codeable-test' ); ?></option>
                    </select>
                </div>
            </div>
            <?php /** create nonce with a random string that we will verify later on. Also add filters in case we need to change method of generation later on **/ ?>
            <?php $ajax_nonce = apply_filters('cs_nonce_creation', wp_create_nonce( "11066e13609c22ee7967561652da1347" ) ); ?>

            <input type="hidden" class="cs-order-paged" name="cs_order_page" value="<?php echo ( $_GET["paged"] ) ? $_GET["paged"] : "1"; ?>"/>
            <input type="hidden" class="cs-security" name="security" value="<?php echo $ajax_nonce; ?>"/>
            <input type="hidden" class="cs-action" name="action" value="<?php _e( apply_filters( "cs_default_action_ajax", $action = "cs_user_return_table" ), 'codeable-test' ) ?>"/>
            <input type="hidden" class="cs-action" name="url_base" value="<?php _e( apply_filters("cs_url_base", $url_base = get_permalink() ), 'codeable-test' ) ?>"/>
        </div>
        <?php do_action("cs_after_form"); ?>
    </form>

    <div class="row" style="margin-top:40px;">
        <div class="col-md-12">
            <table id="cs_codeable_table" class="table table-striped table-bordered" style="width:100%">
                <thead class="thead-dark">
                    <th><?php _e( '#','codeable-test' ); ?></th>
                    <th><?php _e( 'Username','codeable-test' ); ?></th>
                    <th><?php _e( 'Display name','codeable-test' ); ?></th>
                    <th><?php _e( 'Role','codeable-test' ); ?></th>
                </thead>
                <tbody class="population-tbody">
                    <?php echo \CSCodeable\Lib\Users::display_refresh( $users["results"] ); ?>
                </tbody>
                <tfoot class="thead-dark">
                <th><?php _e( '#','codeable-test' ); ?></th>
                <th><?php _e( 'Username','codeable-test' ); ?></th>
                <th><?php _e( 'Display name','codeable-test' ); ?></th>
                <th><?php _e( 'Role','codeable-test' ); ?></th>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right ajax-pagination">
            <?php echo apply_filters( 'cs_custom_pagination', $pages, $page_count ); ?>
        </div>
    </div>
</div>