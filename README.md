# ***View this document using MD ( MarkDown ) compatible viewer.***

# REVISION 1.1
## Comments
- in order for jQuery to use the json response as a json object, you need to use in AJAX wp_send_json otherwise the json encoded string will not be usable by jQuery unless you parse it.
- using json_encode() & wp_die() requires you to $.parseJSON in jQuery
- corrected all typos 
- get_option() swapped for get_user_meta() to prevent unwanted behaviour on multiple admin users
- pagination rebuilt
- authorization validation added to AJAX method
- fixed JS to be ES4 to ES6 compatible
- replaced deprecated .live() handler with .on()
- removed json_encode from get_last_query when $settings is false /null and some other areas.
- swapped manage_options to list_users ( both are only for admins judging by wordpress codex, but it is more relevant to use list_users in this case )
- removed complex DOM/XPath and simplified navigation
- swapped json_encode & wp_die for wp_send_json and altered js accordingly work with the change
##### *I have not implemented a CSS / JS Minification, but my approach would be doing it with matthiasmullie/minify ( https://github.com/matthiasmullie/minify )*

# VERSION 1.0 
## Comments on how it works
- Currently all the sorting is done via **AJAX**, but depending on the user use cases this might not always be good for the server. I would recommend more of the processing to be done on the client side for as much as possible.
- The point above also has restrictions as loading too much data into the **HTML DOM** will cause the user's PC to be slow and possibly cause the browser to crash if we have too many entries listed at once and the filtering might become sluggish. So I would recommend this method for entries that are under let's say ten thousand.
- I have used a localized version of bootstrap with the stock ***Twenty Nineteen theme***. This might cause some of the elements to be viewed differently on other themes ( maybe even have bugs ), thus is why I am mentioning the theme I have used to dev this on. I have tried to make it compatible with more than just this theme, but I did notice some weird altercations on the navigation on Twenty Sixteen for example. But since it wasn't part of the description to make it compatible with multiple themes, I will just go ahead and assume that I have taken this in consideration but will not take action for fixes.

## Specific additions
- Resetting the order and orderby when changing role was pure preference and JS has been adjusted accordingly ( by default it would have kept all fields as they were, meaning order by and order ). To disable this and have that work comment lines **48-53** in _Plugin_>_js_>_custom.js_:(48-53) 
- Added additional feature of saving the last known selection for on refresh loading

## Structure
- I know that the structure might seem a bit complicated but I feel as though it is clean enough to easily grasp it
- To explain it : 
    - Plugin > lib ( this folder contains all base classes for functionality )
    - Plugin > lib > ***App.php*** ( this class instantiates all needed classes )
    - Plugin > css/js ( assets folders that are used to house **CSS/JS** files )
    - Plugin > templates ( folder where we can create more **"view"** files )
    - Plugin > templates > ***table.php*** ( this is where the **HTML for the table** is )
    - Plugin > ***autoload.php*** ( this **function will autoload a class** when required )

## Shortcodes & Instructions
- I remind I would recommend using stock ***Twenty Nineteen theme*** to test out this plugin as it has been built on top of that and it would provide maximum viability
- [table_roles] is the shortcode that we need to use in order for the table to appear on the page