jQuery(function($){

    /** vars used by form **/
    var $role    = $('select.cs-role-select').val();
    var $orderby = $('select.cs-orderby-select').val();
    var $order   = $('select.cs-order-select').val();
    var $paged   = $('.cs-order-paged').val();

    /** update vars function ( to generalize ) **/
    var updateVars = function updateVars ()
    {
        $role    = $('select.cs-role-select').val();
        $orderby = $('select.cs-orderby-select').val();
        $order   = $('select.cs-order-select').val();
        $paged   = $('.cs-order-paged').val();
    };

    /** reset form **/
    $('.cs-reset-form').on( "click", function(e){

        e.preventDefault();

        $('.cs-controller-form').trigger("reset");
        $(document).trigger("cs-Ajax-Get-Table");
    });

    /** on form change perform an attempt to refresh **/
    $('.cs-controller-form select').on("change",function(e){
        e.preventDefault();

        var $this = $(this);

        /** if we're changing the role, reset the other selects as requested **/
        /*** IF YOU WANT COMMENT FROM HERE ***/
        if( $this.hasClass( 'cs-role-select' ) )
        {
            $( '.cs-orderby-select' ).prop( 'selectedIndex', 0 );
            $( '.cs-order-select' ).prop( 'selectedIndex', 0 );
        }
        /*** UNTIL HERE ***/

        $('.cs-order-paged').val("1");
        /** get updated vars **/
        updateVars();
        $(document).trigger('cs-Ajax-Get-Table');
        // alert('role: ' + $role + ' , orderby: ' + $orderby + ' , order: ' + $order + ' , page: ' + $paged);
    });

    /** when navigating through the pages, trigger another form event **/
    $('.ajax-pagination').on( "change", ".page_number_index", function(e){

        e.preventDefault();

        var $this = $(this);
        var $total_pages = $('.cs_codeable_total_pages');

        if( parseInt( $this.val() ) > parseInt( $total_pages.val() ) )
            $this.val( $total_pages.val() );

        if( parseInt( $this.val() ) < 1)
            $this.val( 1 );

        /** assign the page to the form **/
        $('.cs-order-paged').val( $('.page_number_index').val() );

        /** get updated vars **/
        updateVars();

        $(document).trigger('cs-Ajax-Get-Table');
    });


    /** simple navigation left/right **/
    $('.ajax-pagination').on( "click", '.nav_prev_cs',function(e){

        var $page_index = $('input.page_number_index');

        $page_index.val( parseInt( $page_index.val() ) - 1 );
        $page_index.trigger("change");
    });
    $('.ajax-pagination').on( "click", '.nav_next_cs', function(e){

        var $page_index = $('input.page_number_index');

        $page_index.val( parseInt( $page_index.val() ) + 1 );
        $page_index.trigger("change");
    });

    /** submit the ajax form and get updated data **/
    $(document).bind('cs-Ajax-Get-Table', function(e){

        e.preventDefault();

        var $data = $('.cs-controller-form').serialize();

        $.post( ajax_object.ajax_url, $data, function( response ){

            if( response.status === "error_permissions" )
                alert("Sorry, you don't have permissions!");

            $('tbody.population-tbody').html( response.body );
            $('.ajax-pagination').html( response.pagination );
        });

    });

});