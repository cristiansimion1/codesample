<?php
/**
* Plugin Name: Codeable Challenge
* Description: Test project for Codeable
* Version: 1.0.1
* Author: Cristian Simion
* Author URI: http://cristiansimion.com
*/

define("CS_CODEABLE_TEMPLATE_DIR", __DIR__ . '/templates/');
define("CS_CODEABLE_FILE", __FILE__ );
define("CS_CODEABLE_ROOT", __DIR__ );

require_once 'autoload.php';

$app = new \CSCodeable\Lib\App();